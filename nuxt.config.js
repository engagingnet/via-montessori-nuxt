import axios from 'axios';

// import ApolloClient from 'apollo-boost'
// 
module.exports = {

     // mode: 'universal',

     server: {
         host: '0.0.0.0', // default: localhost
     },

    //  target: 'static',

     /*
     ** Uses https://github.com/nuxt/vue-meta
     */
     head: {
          // titleTemplate: '%s | ',
          htmlAttrs: {
               lang: 'en',
          },
          titleTemplate: (titleChunk) => {
               // If undefined or blank then we don't need the hyphen
               return titleChunk ? `${titleChunk} | Via Montessori -  For today’s Casa dei Bambini` : 'Via Montessori - For today’s Casa dei Bambini';
          },
          meta: [
               { charset: 'utf-8' },
               { name: 'viewport', content: 'width=device-width,width=device-width,minimum-scale=1.0,maximum-scale=10.0,initial-scale=1.0' }, // ,minimal-ui
               { name: 'HandheldFriendly', content: 'true' },
               { name: 'MobileOptimized', content: '320' },
               { hid: 'description', name: 'description', content: "Exploring Dr Maria Montessori’s way of nurturing our children." },
               { name: 'msapplication-TileColor', content: '#27bfff' },
               { name: 'theme-color', color: '#c9774f' },
          ],
          script: [
               // Can alreaey see slowdown to load, and don't know how to invoke
               //     { src: '//cdn.jsdelivr.net/npm/balance-text@3.2.1/balancetext.min.js' }
          ],
          link: [
//               { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
               { rel: 'icon', type: 'image/x-icon', size: '32x32', href: '//res.cloudinary.com/via-montessori/image/upload/favicon-32x32.png' },
               { rel: 'icon', type: 'image/png', size: '16x16', href: '//res.cloudinary.com/via-montessori/image/upload/favicon-16x16.png' },
               { rel: 'apple-touch-icon', size: '180x180', href: '//res.cloudinary.com/via-montessori/image/upload/apple-touch-icon.png' },
               { rel: 'manifest', size: '180x180', href: '/site.webmanifest' },
               { rel: 'mask-icon', color: '#5bbad5', href: '/safari-pinned-tab.svg' },

               { rel: 'stylesheet', type: 'text/css', href: '//cloud.typography.com/6106452/7803592/css/fonts.css' },

               { rel: 'alternate', type: 'application/rss+xml', href: '/feed', title: 'RSS' },

          //     { type: 'text/js', href: 'https://cdnjs.cloudflare.com/ajax/libs/balance-text/3.2.1/balancetext.min.js' },


          ]
     },

     /*
     ** Customize the progress-bar color
     */
     loading: false,

     /*
     routes: [
          // aha yes this is where we will combine to a general page?
          {
               path: '/articles/:Slug',
               name: 'articles',
          },
          {
               path: '/learning-areas/:Slug',
               name: 'learning-areas',
          },
     ],
     */

     // Because it seems to lose it on production
     dir: {
         static: 'static',
     },

     /*
     ** Global CSS
     */
     css: [
          '@/assets/main.scss',
          '@/assets/typography.scss',
          '@/assets/backgrounds.scss',
          '@/assets/page-transition.css',
          '@/assets/tooltip.scss',
     ],

     /*
     ** Plugins to load before mounting the App
     */
     plugins: [
          { src: '~/plugins/intersection-observer', ssr: false }, // For Safari to use lazyload
          { src: '~/plugins/v-lazy-image' } , // ssr: false }, // https://github.com/hilongjw/vue-lazyload
          { src: '~/plugins/v-tooltip' },
          // { src: '~/plugins/vue-fontawesome' }, absolutely huge
          // { src: '~/plugins/vue-markdown' },
          { src: '~/plugins/vue-moment' },
          { src: '~/plugins/vue-scrollto' },
          // { src: '~/plugins/vue-lazyload' } , // ssr: false }, // https://github.com/hilongjw/vue-lazyload
          // { src: '~/plugins/vue-titlecase' }, doesnt work
          // { src: '~/plugins/vue-balance-text'} // Error: "balanceText is not a function" already got ssr: false in the plugin
     ],

     /*
     ** Nuxt.js modules
     */
     modules: [

          ['@nuxtjs/feed'],

          ['@nuxtjs/style-resources'], // This is the new one, even though it seems to bloody load everything repeatedly unnecessarily

          ['@nuxtjs/apollo', {
               clientConfigs: {
                    default:  '~/plugins/apollo-default-config.js'
               },
               includeNodeModules: true
          }],

          ['nuxt-fontawesome', {
               component: 'fa',
               imports: [
               //import whole set
               {
                    set: '@fortawesome/pro-solid-svg-icons',
                    icons: [ 'faChevronCircleUp', 'faTh', 'faArrowsAltH', 'faRss', 'faRssSquare'],
               },
               /* Don't need but this would be how
               {
                    set: '@fortawesome/pro-light-svg-icons',
                    icons: ['faTh'],

               }
               */
               ]
          }],

          ['@nuxtjs/markdownit'],

          ['vue-balance-text/nuxt/module'],


                    /*
                    ['@reallifedigital/nuxt-image-loader-module', {
                         imagesBaseDir: 'content',
                         // imagesBaseDir: 'http://localhost:1337/uploads/',
                         imageStyles: {
                              museum_hover: { actions: ['resize|10|224^', 'quality|85'] },
                              // museum: { actions: ['modulate|115|0|100', 'colorize|7|21|50', 'resize|10|224^', 'quality|85'] },
                              museum: { actions: ['resize|10|224^', 'quality|85'] },
                              thumbnail: { actions: ['gravity|Center', 'resize|320|180^', 'extent|320|180|+0|+90'] },
                              small: { macros: ['scaleAndCrop|160|90'] },
                              medium: { macros: ['scaleAndCrop|320|180'] },
                              large: { macros: ['scaleAndCrop|640|360'] },
                         },
                    }],
                    */

                    // Stops graphql content loading eventually
                    // [ 'vue-balance-text/nuxt/module' ]

     ],

     feed: [
          {
               path: '/feed', // The route to your feed.
               type: 'rss2',
               // cacheTime: 1000 * 60 * 15,
               cacheTime: 1,
               // data: ['Some additional data'], // Will be passed as 2nd argument to `create` function

               async create(feed) {

                    feed.options = {
                         title: 'Via Montessori',
                         description: 'For today’s Casa dei Bambini',
                         link: 'https://via-montessori.com',
                         language: 'en',
                         image: 'https://res.cloudinary.com/via-montessori/image/upload/via-montessori-logo.png',
                         favicon: 'https://res.cloudinary.com/via-montessori/image/upload/favicon-32x32.png',
                         generator: "https://github.com/nuxt-community/feed-module and https://strapi.io/",
                    }

                    // Grab our various entries
                    const activity_posts = await (axios.get('http://localhost:1337/activities?Status_eq=Open&_sort=Publish_Date:DESC&_limit=10'));
                    const article_posts = await (axios.get('http://localhost:1337/articles?Status_eq=Open&_sort=Publish_Date:DESC&_limit=10'));
                    let activity_entries = activity_posts.data;
                    let article_entries =  article_posts.data;

                    // Combine our various entries
                    let entries = activity_entries.concat(article_entries);

                    // Sort our combined entries
                    entries.sort((a, b) => (a.Publish_Date < b.Publish_Date) ? 1 : -1);

                    // Loop through 10 of our entries
                    entries.slice(0, 10).forEach(post => {

                         let authors = post.Authors;
                         let ourAuthors = [];
                         authors.forEach(author => {
                              ourAuthors.push({
                                   name: author.username,
                                   email: author.email,
                                   link: 'https://via-montessori.com/educators/' + author.Slug,
                              })
                         });

                         // Process our image
                         let images = post.Images;
                         let ourImage = '';
                         let i = 0;
                         images.forEach(image => {
                              if (i < 1) {
                                   ourImage = image.url;
                              }
                              i++;
                         });

                         feed.addItem({
                              date: new Date( post.Publish_Date ),
                              title: post.Title,
                              guid: post.id,
                              link: 'https://via-montessori.com/activities/' + post.Slug,
                              description: post.Blurb,
                              author: ourAuthors,
                         //     content: post.Images.url,
                              categories: [ 'Life Skills activities' ],
                              image: ourImage
                         })
                    });
               },
          },
     ],

     markdownit: {
          preset: 'default',
          typographer: true,
          injected: true,
          html: true,
          quotes: '“”‘’',
       },

     styleResources: {
          scss: [
               '@/assets/breakpoints.scss',
               '@/assets/colors.scss',
               '@/assets/fonts.scss',
          ]
     },

     /*
     ** Build configuration
     */
     build: {
     /*
     ** You can extend webpack config here
     */

          // analyze: true,

          /*
          extend (config, { isDev, isClient }) {

               if (isDev && isClient) {
                    config.module.rules.push({
                         enforce: 'pre',
                         test: /\.(js|vue)$/,
                         loader: 'eslint-loader',
                         exclude: /(node_modules)/
                    });
               }

               // Not doing this, got cloudinary instead
               // https://stackoverflow.com/questions/48606325/how-to-resize-images-for-different-responsive-views
          },
          */
     },

}