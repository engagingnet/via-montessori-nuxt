import Vue from 'vue';
import { VTooltip, VPopover, VClosePopover } from 'v-tooltip'

Vue.directive('tooltip', VTooltip, { defaultDelay: 500 }) // delay not happenin
Vue.directive('close-popover', VClosePopover)
Vue.component('v-popover', VPopover)