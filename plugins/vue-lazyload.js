import Vue from 'vue';
import VueLazyload from 'vue-lazyload';

Vue.use(VueLazyload, {
     lazyComponent: true,
     preLoad: 0.62, // proportion of pre-loading height
     observer: true,
     observerOptions: {
          rootMargin: '500px',
          threshold: 0.1
     }
})