<h1>Via Montessori</h1>

<h2>What is it?</h2>

Via Montessori is a web site by <a href="http://engaging.net">Engaging</a> for Montessori teacher Roberta Frosolini about Montessori educational values and methods.

<h2>How is it built?</h2>

<a href="https://nuxtjs.org">Nuxt</a> serves as the site’s framework, <a href="https://strapi.io">Strapi</a> as its headless content management system. The two are connected using <a href="https://graphql.org">GraphQL</a> by means of <a href="https://apollographql.com">Apollo</a>.

<h2>To whom might this be of interest?</h2>

Web developers wishing to build using this stack might find it a helpful example.